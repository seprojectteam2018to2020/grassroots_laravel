<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    function __construct()
    {
        $this->tableName = config('variables.tables_name')['011'];
        $this->tableName_type = config('variables.tables_name')['013'];
    }

    public function up()
    {
        Schema::dropIfExists($this->tableName_type);
        Schema::create($this->tableName_type, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::dropIfExists($this->tableName);
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('_users')
                ->onDelete('cascade');

            $table->integer('priority')->default(0);
            $table->unsignedInteger('feedback_type_id');
            $table->foreign('feedback_type_id')
                ->references('id')
                ->on($this->tableName_type)
                ->onDelete('cascade');
            $table->text('content');
            $table->text('chat');
            $table->text('files');
            $table->integer('status')->default(10);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
        Schema::dropIfExists($this->tableName_type);
    }
}
