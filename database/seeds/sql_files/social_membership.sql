-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1:3306
-- 產生時間： 
-- 伺服器版本： 5.7.26
-- PHP 版本： 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+8:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `social_membership`
--

-- --------------------------------------------------------

INSERT INTO `setting_currency` (`id`, `currency`, `rate`, `valid_from`, `valid_to`, `status`, `created_at`, `updated_at`) VALUES
(1, 'HKD', 1, NULL, NULL, 1, '2019-09-02 09:34:32', '2019-09-02 09:34:32'),
(2, 'RMB', 1.12, NULL, NULL, 1, '2019-09-02 09:34:32', '2019-09-02 09:34:32'),
(3, 'USD', 7.8, NULL, NULL, 1, '2019-09-02 09:34:32', '2019-09-02 09:34:32'),
(4, 'EUR', 8.7, NULL, NULL, 1, '2019-09-02 09:34:32', '2019-09-02 09:34:32'),
(5, 'GBP', 9.5, NULL, NULL, 1, '2019-09-02 09:34:32', '2019-09-02 09:34:32');

INSERT INTO `setting_feedback_type` (`id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Feedback', 1, NULL, '2019-09-30 10:37:16', '2019-09-30 10:37:16'),
(2, 'Internet service', 1, NULL, '2019-09-30 10:37:16', '2019-09-30 10:37:16'),
(3, 'Communication service', 1, NULL, '2019-09-30 10:37:16', '2019-09-30 10:37:16'),
(4, 'System Tool', 1, NULL, '2019-09-30 10:37:16', '2019-09-30 10:37:16'),
(5, 'Security', 1, NULL, '2019-09-30 10:37:16', '2019-09-30 10:37:16'),
(6, 'Data Storage', 1, NULL, '2019-09-30 10:37:16', '2019-09-30 10:37:16'),
(7, 'Subscription & Payment', 1, NULL, '2019-09-30 10:37:16', '2019-09-30 10:37:16'),
(8, 'Other', 1, NULL, '2019-09-30 10:37:16', '2019-09-30 10:37:16');

INSERT INTO `setting_event_type` (`id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Activity', 1, '2019-09-02 09:34:33', '2019-09-02 09:34:33'),
(2, 'Lecture', 1, '2019-09-02 09:34:33', '2019-09-02 09:34:33');



INSERT INTO `_users` (`id`, `name`, `email`, `email_verified_at`, `password`, `address`, `birth`, `status`, `point`, `phone`, `avatar`, `currency`, `remarks`, `api_token`, `added_at`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'a:2:{s:5:\"first\";s:3:\"Ken\";s:4:\"last\";s:2:\"So\";}', 'admin@test.com', NULL, '$2y$10$ZuBuHKne0s.SmPx40mp/IOIUpDGuo9eaXd8DaAPhe.a1JDzDqKXMy', '6778 Grant Port Apt. 541\nClarabelleland, RI 58965-5541', '2001-07-08', 1, 100, '1-678-974-6325 x344', 'icon.png', 1, '', 'Pr6cwRomXkXXEm1jioUYDl9e2DGxDt36PY19XMUrlwAPtNCBf36OxnR6mstvbRmf', '2019-09-03', NULL, NULL, '2019-09-30 10:39:20', '2019-09-30 10:39:20'),
(2, 'a:2:{s:5:\"first\";s:5:\"David\";s:4:\"last\";s:3:\"Lee\";}', 'user2@test.com', NULL, '$2y$10$S/N55qkV/6VRBimUseIhEuGb.jl88rSBrEAKwSXZqWYjJoHx4qog6', '51284 Trantow Key Suite 733New Teresa, HI 15013', '2018-07-08', 1, 100, '60582734', 'icon.png', 1, 'Hello, My company want to join the social membership team', 'r7bBCpKeWJX4liW38ZseUx4t81i6wVGa0VIK8UhKE9ecTHiC4EcJjp4q2CQCFQFf', '2019-09-03', NULL, NULL, '2019-09-30 10:39:20', '2019-09-30 10:39:20'),
(3, 'a:2:{s:5:\"first\";s:5:\"Ethel\";s:4:\"last\";s:7:\"Johnson\";}', 'user3@test.com', NULL, '$2y$10$ZfQWMkzCesrejc6amRNkdOQ7FkXpVUHOc1tHimfXj6TBSmmdMv4y.', '1735 Konopelski CovesHilpertside, UT 73662-2044', '2017-05-14', 1, 100, '59382734', 'icon.png', 1, 'I am new to this area', 'EP7isbQjJnpZg4gwf81ufJjM4iRhEDcp8veCqQwJp3ouV6yEvzpJihyRw4QTEFjC', '2019-09-03', NULL, NULL, '2019-09-30 10:39:20', '2019-09-30 10:40:21'),
(4, 'a:2:{s:5:\"first\";s:7:\"Latrice\";s:4:\"last\";s:5:\"Musso\";}', 'user4@test.com', NULL, '$2y$10$8GrvC7qQ3ZHWPWHlxmjbSOCCjp8mETWNoS9UfhEdXRIAtfRuRnTxS', '87734 Rhianna Terrace Suite 351North Wendellland, ID 13901-2864', '2010-07-27', 2, 100, '+1.238.817.7346', 'icon.png', 1, 'Hello', 'Kgah9KkpMU7KGuRjpaBvVKZFsOA3erWteGibTjLNSPb5kYvwKTRZzRo1hR265gVH', '2019-09-03', NULL, NULL, '2019-09-30 10:39:20', '2019-09-30 10:41:39'),
(5, 'a:2:{s:5:\"first\";s:4:\"Gary\";s:4:\"last\";s:6:\"Coffey\";}', 'user5@test.com', NULL, '$2y$10$Qwm.wbIk5pE.kP5acmq4TeXdaL/aYKc6WSH0.SnoG.l5ss7o303ca', '837 Lynch Square Apt. 053Caesarfort, ME 81757-0349', '2017-01-23', 0, 100, '59837102', 'icon.png', 1, 'Hello, Nice to meet you', 'ZujADcM0SHZHXaUM5mjqhCw4O8zINT1C0r7tUYvDkZ6urP1yTmdjSNKoOgTPSAm1', NULL, NULL, NULL, '2019-09-30 10:39:20', '2019-09-30 10:41:19');


INSERT INTO `events` (`id`, `title`, `content`, `image`, `reward`, `type`, `limit_of_apply`, `venue`, `time`, `valid_from`, `valid_to`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Moon cake workshop', '【Mooncake Festival Can’t Be without Mooncake】\r\n\r\nIn Chinese culture, Mid-Autumn Festival, aka Mooncake Festival, would not be completed without mooncakes. Although nowadays you can almost purchase any kinds of flavours at bakeries or restaurants, making them yourself is a different experience and the recipients would definitely appreciate more than those one bought from popular retail stores.\r\n\r\nThis time, we will have Miss Fong from We R Family to teach us how to make some creamy custard mooncakes and snow skin mooncakes. If you want to learn it, don’t miss this workshop!\r\n\r\nOnly limited seats available, Register NOW!\r\n', 'ev1.jpg', 80, 2, 40, 'Tuen Mum VTC campus', '2019-04-05 20:00 - 22:00', '2019-09-01', '2019-09-05', NULL, '2019-09-30 10:39:20', '2019-09-30 10:39:20'),
(2, 'Fire Dragon Dance', 'In the 19th century, the people of Tai Hang began performing a dragon dance to stop a run of bad luck afflicting their village. More than a century later, their village has been all but swallowed up by Hong Kong’s fast-growing city. But the dragon keeps on dancing. It has even danced its way onto China’s third national list of intangible cultural heritage and been selected as one of the Remarkable Examples of Good Practice for Safeguarding the National ICH Items in recognition of the efforts in safeguarding this intangible cultural heritage.\r\n\r\nAll this started a few days before the Mid-Autumn Festival, sometime around 100 years ago. First a typhoon slammed into the fishing and farming community of Tai Hang. This was followed by a plague, and then when a python ate the villagers’ livestock, they said enough was enough. A soothsayer decreed the only way to stop the chaos was to stage a fire dance for three days and nights during the upcoming festival. The villagers made a huge dragon from straw and covered it with incense sticks, which they then lit. Accompanied by drummers and erupting firecrackers, they did what they were told and danced for three days and three nights — and the plague disappeared.\r\n\r\nTai Hang may no longer be a village, but its locals still recreate the fiery ancient ritual to this day with a whopping dragon that is not to be taken lightly! On the day before the Mid-Autumn Festival, dragon dance participants perform a series of rituals in the Hakka dialect at Lin Fa Temple, a Declared Monument known for its unique East-meets-West style architectural features. The commemorative performance then wends its way in fire, smoke and festive fury through the backstreets of Tai Hang over three moon-fuelled days.', 'ev2.jpg', 100, 1, 30, 'Tuen Mum VTC campus', '2019-07-12 18:00 - 20:00', '2019-08-25', '2019-09-14', NULL, '2019-09-30 10:39:20', '2019-09-30 10:39:20'),
(3, 'Guess riddle', 'Do you want to feel fun and learn more culture with the Mid Autumn Festival?\r\n\r\nHere is the chance!\r\n\r\nCome here and join the English guess riddle', 'ev3.jpg', 300, 1, 45, 'Tuen Mum VTC campus', '2019-07-23 20:00 - 22:00', '2019-08-29', '2019-09-14', NULL, '2019-09-30 10:39:20', '2019-09-30 10:39:20'),
(4, 'Mid-Autumn Festival and Lantern Carnivals', 'The Mid-Autumn Festival and Lantern Carnivals in Hong Kong take place in many parts of the territory and run for a few days before and after the festival proper which is celebrated on the 8th Full Moon in the Lunar Calendar, usually around September/October in the Western calendar.\r\n\r\nTogether with the Tai Hang Fire Dragon Dance celebrations, they are the centerpieces of the festivities of the Mid-Autumn festival or Moon festival as it is also known.\r\n\r\nOn this full moon day, families and communities traditionally gathered to celebrate the bountiful harvesting season, somewhat of the equivalent to the American Thanksgiving Day. Nowadays, families and friends take part in the many lantern carnivals and celebrations around the city.\r\n\r\nThe Lantern Carnivals, besides showcasing an incredible variety of colorful, bright shiny lanterns, feature also many cultural events and performances including song and dances, kung fu, acrobatics, craft demonstrations and the like. If you happen to be in town during this period, be sure to check them out!\r\n\r\nThe Hong Kong Leisure and Cultural Services Department (LCSD) sponsors many of the events. The displays and cultural activities are free to the public.', 'ev4.png', 150, 1, 15, 'Tuen Mun VTC campus', '2019-09-05 14:00 - 16:00', '2019-09-04', '2019-09-15', NULL, '2019-09-30 10:39:20', '2019-09-30 10:39:20'),
(5, 'Talent Show', 'We will have a talent show between the lantern festival, You can join and play your performance to entertain the audience. They will vote the best performer. We have prepare the gift and reward to the First, Second and Third Place.\r\n\r\nPlease feel free to join us.', 'ev2.jpg', 200, 2, 20, 'Tuen Mun VTC campus', '2019-07-11 18:00 - 22:00', '2019-08-23', '2019-09-10', NULL, '2019-09-30 10:39:20', '2019-09-30 10:39:20');




-- INSERT INTO `notifications` (`id`, `title`, `sub_heading`, `content`, `image`, `attachment`, `location`, `date`, `type`, `deleted_at`, `created_at`, `updated_at`) VALUES
-- (1, 'New mid autumn festival event is up!', 'Mooncake', 'There is a mooncake workshop is coming up, please feel free to check the detail.', 'dragon_dance.jpg', NULL, 'TM VTC campus', '2019-08-29', 2, NULL, '2019-09-30 10:39:20', '2019-09-30 10:39:20'),
-- (2, 'Introducing happy new year in anew light', 'Dragon Dance is coming', 'There is a Fire Dragon dance performance is coming, We have invited the popular dragon dancing team to perform.\r\nPlease feel free to check the detail.', 'lantern.jpg', NULL, 'The Hong Kong Polytechnic University', '2019-08-29', 1, NULL, '2019-09-30 10:39:20', '2019-09-30 10:39:20'),
-- (3, 'The new way to use upgrade is epic', 'Guessing Riddle', 'To let you guys enjoy the mid autumn festival, we have prepared a large English Riddle on the  hall. We will have around 200 riddles, and prepare a reward for guessing king/queen.\r\nPlease feel free to check the details.', 'mookcake.png', NULL, 'Lee Wai Lee Technical Institute', '2019-08-29', 1, NULL, '2019-09-30 10:39:20', '2019-09-30 10:39:20'),
-- (4, '5 things you should do to achieve dinner', 'Lantern Carnivals', 'We have invited Lantern Team to performs Lantern Carnivals between Moon Festival Party. \r\nPlease look the details.', 'riddle.jpg', NULL, 'The University of Hong Kong', '2019-08-29', 1, NULL, '2019-09-30 10:39:20', '2019-09-30 10:39:20'),
-- (5, 'Welcome in wonderland: a taleof two worlds', 'Talent show', 'We have a talent show between Moon Festival Party. If you have interest to entertain the audience. Please come to join us.', 'talent.jpg', NULL, 'City University of Hong Kong', '2019-08-29', 2, NULL, '2019-09-30 10:39:20', '2019-09-30 10:39:20');

INSERT INTO `notification_users` (`id`, `user_id`, `notification_id`, `title`, `subtitle`, `read`, `created_at`, `updated_at`) VALUES
(2, 2, NULL, 'Administrator has response your feedback, please check it in the app', 'Dear Sir/Madam, \r\nThank you for your feedback, We would contect our technician as soon as possible, sorry for causing your troubles. \r\nYours truly, \r\nMr. Ken', 1, '2019-09-30 12:05:15', '2019-09-30 12:05:31'),
(3, 2, NULL, 'Your expense is not approved', 'You cannot claim the budgets', 0, '2019-09-30 12:13:55', '2019-09-30 12:13:55'),
(4, 2, NULL, 'Your expense is not approved', 'Please see the comment', 0, '2019-09-30 12:45:12', '2019-09-30 12:45:12');


--
-- 傾印資料表的資料 `_model_has_roles`
--

INSERT INTO `_roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(2, 'User', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(3, 'Staff', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(4, 'Member', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(5, 'Junior Member', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(6, 'Senior Member', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32');



INSERT INTO `_model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(3, 'App\\Models\\User', 1),
(5, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 2),
(4, 'App\\Models\\User', 2),
(2, 'App\\Models\\User', 3),
(4, 'App\\Models\\User', 3),
(2, 'App\\Models\\User', 4),
(4, 'App\\Models\\User', 4),
(2, 'App\\Models\\User', 5);


INSERT INTO `_permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'role-list', 'web', '2019-09-02 17:34:31', '2019-09-02 17:34:31'),
(2, 'role-create', 'web', '2019-09-02 17:34:31', '2019-09-02 17:34:31'),
(3, 'role-edit', 'web', '2019-09-02 17:34:31', '2019-09-02 17:34:31'),
(4, 'role-delete', 'web', '2019-09-02 17:34:31', '2019-09-02 17:34:31'),
(5, 'user-list', 'web', '2019-09-02 17:34:31', '2019-09-02 17:34:31'),
(6, 'user-create', 'web', '2019-09-02 17:34:31', '2019-09-02 17:34:31'),
(7, 'user-edit', 'web', '2019-09-02 17:34:31', '2019-09-02 17:34:31'),
(8, 'user-delete', 'web', '2019-09-02 17:34:31', '2019-09-02 17:34:31'),
(9, 'user-profile', 'web', '2019-09-02 17:34:31', '2019-09-02 17:34:31'),
(10, 'reward-list', 'web', '2019-09-02 17:34:31', '2019-09-02 17:34:31'),
(11, 'reward-create', 'web', '2019-09-02 17:34:31', '2019-09-02 17:34:31'),
(12, 'reward-edit', 'web', '2019-09-02 17:34:31', '2019-09-02 17:34:31'),
(13, 'reward-delete', 'web', '2019-09-02 17:34:31', '2019-09-02 17:34:31'),
(14, 'event-list', 'web', '2019-09-02 17:34:31', '2019-09-02 17:34:31'),
(15, 'event-create', 'web', '2019-09-02 17:34:31', '2019-09-02 17:34:31'),
(16, 'event-edit', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(17, 'event-delete', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(18, 'notification-list', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(19, 'notification-create', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(20, 'notification-edit', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(21, 'notification-delete', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(22, 'feedback-list', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(23, 'feedback-create', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(24, 'feedback-edit', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(25, 'feedback-delete', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(26, 'receipt-list', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(27, 'receipt-create', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(28, 'receipt-edit', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(29, 'receipt-delete', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(30, 'event-add', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(31, 'event-withdraw', 'web', '2019-09-02 17:34:32', '2019-09-02 17:34:32');



INSERT INTO `_role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(24, 1),
(26, 1),
(28, 1),
(1, 2),
(9, 2),
(14, 2),
(18, 2),
(22, 2),
(23, 2),
(24, 2),
(25, 2),
(26, 2),
(27, 2),
(28, 2),
(29, 2),
(30, 2),
(31, 2);





INSERT INTO `user_point_records` (`id`, `user_id`, `content`, `points`, `type`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'New User Rewards Package', 100, 1, NULL, '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(2, 2, 'New User Rewards Package', 100, 1, NULL, '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(3, 3, 'New User Rewards Package', 100, 1, NULL, '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(4, 4, 'New User Rewards Package', 100, 1, NULL, '2019-09-02 17:34:32', '2019-09-02 17:34:32'),
(5, 5, 'New User Rewards Package', 100, 1, NULL, '2019-09-02 17:34:32', '2019-09-02 17:34:32');


INSERT INTO `receipts` (`id`, `title`, `user_id`, `transacted_at`, `currency_id`, `location`, `expense_type`, `payment_type`, `amount`, `image`, `status`, `remarks`, `comment`, `deleted_at`, `created_at`, `updated_at`) VALUES
(3, 'Transport for delivery', 2, '2019-09-29', 1, 1, 1, 1, 169.4, '7Mkr5fTXzzsJdPcU.jpg', 2, 'Our Company delivery cost', 'Your receipt image is not relevant', NULL, '2019-09-30 12:12:06', '2019-09-30 12:13:55'),
(4, '義賣物資', 2, '2019-09-28', 1, 1, 2, 2, 1400, '', 2, '幫助殘障人士', 'No photo', NULL, '2019-09-30 12:20:54', '2019-09-30 12:45:12');


INSERT INTO `shop_item` (`id`, `title`, `points`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Travel Insurance', 20, 'free 4 days 3 nights insurance', '2019-09-17 11:14:11', '2019-09-11 13:11:11');

INSERT INTO `feedbacks` (`id`, `user_id`, `priority`, `feedback_type_id`, `content`, `chat`, `files`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(4, 2, 0, 8, 'a:2:{s:5:\"title\";s:25:\"I can not edit my profile\";s:4:\"body\";s:24:\"I want to change my name\";}', 'a:1:{i:0;a:3:{s:4:\"user\";s:5:\"admin\";s:4:\"body\";s:157:\"Dear Sir/Madam, \r\nThank you for your feedback, We would contect our technician as soon as possible, sorry for causing your troubles. \r\nYours truly, \r\nMr. Ken\";s:10:\"created_at\";i:1569845115;}}', 'a:0:{}', 10, NULL, '2019-09-30 12:04:58', '2019-09-30 12:05:15');



COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
