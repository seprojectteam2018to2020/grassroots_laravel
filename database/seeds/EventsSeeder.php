<?php

use App\Models\Event\Event;
use App\Models\Event\EventUser;
use App\Models\Settings\EventType;
use Illuminate\Database\Seeder;

class EventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        Event::truncate();
        EventUser::truncate();
        EventType::truncate();
        $data = [];
        $dataUser = [];

        EventType::create([
            'type'  =>  'News',
        ]);
        EventType::create([
            'type'  =>  'Events',
        ]);

        for ($i = 1; $i <= 5; $i++) {
            array_push($data, [
                'id'                => $i,
                'title'             => $faker->sentence(),
                'content'           => $faker->text(),
                'type'              => $type=EventType::all()->random()->id,
                'reward'            => $faker->numberBetween($min=10,$max=50),
                'limit_of_apply'    => ($type==1)?null:$faker->numberBetween($min=5,$max=50),
                'venue'             => 'Tuen Mum VTC campus',
                'time'              => '20:00 - 22:00'
            ]);
            // array_push($dataUser, [
            //     'event_id' => $i,
            //     'user_id' => '2',
            //     // 'status' => '1',
            // ]);
        }
        Event::insert($data);
        // EventUser::insert($dataUser);
    }
}