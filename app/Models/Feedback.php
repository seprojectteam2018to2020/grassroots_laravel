<?php

namespace App\Models;

use App\Models\Settings\FeedbackType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];

    public function getTable()
    {
        return config('variables.tables_name')['011'];
    }

    public function getContentAttribute($value)
    {
        return unserialize($value);
    }
    public function getChatAttribute($value)
    {
        return unserialize($value);
    }
    public function getFilesAttribute($value)
    {
        return unserialize($value);
    }

    public function user()
    {
        return $this->belongsTo(User::Class);
    }

    public function feedbackType()
    {
        return $this->belongsTo(FeedbackType::Class);
    }
}
