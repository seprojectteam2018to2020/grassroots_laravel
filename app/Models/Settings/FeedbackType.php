<?php

namespace App\Models\Settings;

use App\Models\Feedback;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FeedbackType extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];

    public function getTable()
    {
        return config('variables.tables_name')['013'];
    }

    public function Feedback()
    {
        return $this->hasMany(Feedback::Class, 'type');
    }
}
