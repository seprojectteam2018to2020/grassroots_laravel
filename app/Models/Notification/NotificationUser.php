<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class NotificationUser extends Model
{

    protected $guarded = [];
    protected $hidden = ['updated_at'];

    public function getTable()
    {
        return config('variables.tables_name')['015'];
    }

    public function getCreatedAtAttribute($value)
    {

        if (Carbon::now() > Carbon::parse($value)->addDays(15)) {
            return Carbon::parse($value)->toDateTimeString();
        }

        return Carbon::parse($value)->diffForHumans();
    }
}
