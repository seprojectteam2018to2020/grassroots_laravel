<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class NotificationDevice extends Model
{
    protected $keyType = 'string';
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];

    public function getTable()
    {
        return config('variables.tables_name')['007'];
    }

    public function user()
    {
        return $this->belongsTo(User::Class);
    }
}
