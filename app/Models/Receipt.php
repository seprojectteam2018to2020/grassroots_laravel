<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receipt extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];

    public function getTable()
    {
        return config('variables.tables_name')['010'];
    }

    public function getImageAttribute($value)
    {
        if ($value != "") {
            return config('variables.image.receipt') . $value;
        } else {
            return null;
        }
    }

    protected $casts = [
        'expense_type' => 'string',
        'payment_type' => 'string',
        'location' => 'string',
        'currency_id' => 'string',
        'amount' => 'string'
    ];
}
