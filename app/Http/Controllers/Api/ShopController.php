<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\ShopItem;
use App\Models\UserPointRecord;
use Illuminate\Support\Facades\Auth;

class ShopController extends ApiController
{
    public function index()
    {
        try {
            $query = ShopItem::orderBy('created_at', 'desc')->get();
            return parent::sendResponse('data', $query, 'Shop Item Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function exchange(Request $request, $id)
    {
        $user = Auth::guard('api')->user();
        $item_point = ShopItem::findOrFail($id)->points;

        if ($item_point > $user->point) {
            return parent::sendError('You do not have enough credit.', 216);
        }

        if (
            $user->update(['point' => $user->point - $item_point]) &&
            UserPointRecord::create([
                'user_id'   => $user->id,
                'content'   => 'Exchange Shop Item',
                'points'    => $item_point,
                'type'      => false,
            ])
        ) {
            return parent::sendResponse('status', "success", 'Exchange Success');
        } else {
            return parent::sendError('Cannot Exchange', 215);
        }
    }
}
