<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Feedback;
use Illuminate\Support\Facades\Auth;
use App\Models\Settings\FeedbackType;

class FeedbackController extends ApiController
{
    public function index(Request $request)
    {
        try {
            $query = Feedback::all();
            return parent::sendResponse('data', $query, 'Feedback Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function create(Request $request)
    {
        $feedback = new Feedback();
        $files_array = array();
        if (isset($request->image)) {
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileNameToStore = str_random(16) . '_' . time() . '.' . $extension;
            $request->file('image')->storeAs('feedback', $fileNameToStore);
            array_push($files_array, $fileNameToStore);
            $feedback->files = serialize($files_array);
        } else {
            $feedback->files = serialize(array());
        }

        $feedback->user_id = Auth::guard('api')->user()->id;
        $feedback->feedback_type_id = $request->type;
        $feedback->content = serialize(array('title' => $request->title, 'body' => $request->content));
        $feedback->chat = serialize(array());

        if ($feedback->save()) {
            return parent::sendResponse('status', "success", 'Create successfully.');
        } else {
            return parent::sendError('Cannot create, please check your information again', 215);
        }
    }

    public function feedback_type_list()
    {
        try {
            $query = FeedbackType::all();
            return parent::sendResponse('data', $query, 'Feedback Type Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }
}
