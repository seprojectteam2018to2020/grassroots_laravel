<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\UserPointRecord;
use App\Models\Notification\NotificationDevice;

class AuthController extends ApiController
{

    public function login(Request $request)
    {
        try {
            $user = \DB::table('_users')->where('email', $request->email)->first();

            if (empty($user)) {

                return parent::sendError('You input wrong email, please try again.', 215);
            } else if (!\Hash::check($request->password, $user->password)) {

                return parent::sendError('You input wrong passowrd, please try again.', 215);
            } else {

                if ($user->status != 2) {
                    return parent::sendResponse('token', $user->api_token, 'Logined');
                } else {
                    return parent::sendError('You account have been blocked.', 219);
                }
            }
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }



    public function register(Request $request)
    {

        try {
            if (User::where('email', $request->email)->first() != null) {
                return parent::sendError('Email address has been used, please try another address', 225);
            }

            $token = str_random(64);
            $new_user = User::create([
                'email' => $request->email,
                'password' => $request->password,
                'name' => serialize([
                    'first' => $request->first_name,
                    'last' => $request->last_name,
                ]),
                'birth' => $request->birth,
                'phone' => $request->phone,
                'address' => $request->address,
                'remarks' => $request->remarks,
                'api_token' => $token // hash('sha256', $token)
            ]);

            $new_user->assignRole('User');

            if (isset($new_user)) {
                return parent::sendResponse('token', $token, 'Register successfully.');
            } else {
                return parent::sendError('Cannot register, please check your information again', 215);
            }
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function profile()
    {
        try {

            $user_id = Auth::guard('api')->user()->id;
            $query = User::where('id', $user_id)->first();
            $query->role = Auth::guard('api')->user()->roles->pluck('name');
            return parent::sendResponse('data', $query, 'Profile Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function edit_profile(Request $request)
    {
        try {
            $user = User::find(Auth::guard('api')->user()->id);
            if (isset($request->new_avatar)) {
                $extension = $request->file('new_avatar')->getClientOriginalExtension();
                $fileNameToStore = str_random(16) . '_' . time() . '.' . $extension;
                $request->file('new_avatar')->storeAs(config('variables.avatar.folder'), $fileNameToStore);
                $user->avatar  = $fileNameToStore;
            }

            $user->address  = $request->address;
            $user->birth    = $request->birth;
            $user->phone    = $request->phone;
            $user->remarks  = $request->remarks;

            if (isset($request->new_password)) {
                $user->password = $request->new_password;
            }

            $user->save();
            return parent::sendResponse('message', 'success', 'Profile Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function points_record()
    {
        try {
            $user_id = Auth::guard('api')->user()->id;
            $query = User::where('id', $user_id)->first();
            $roles = \DB::table('_model_has_roles')->where('model_type', 'like', '%User')->where('model_id', $user_id)->get();
            $roleName = array();
            foreach ($roles as $role) {
                array_push($roleName, Role::where('id', $role->role_id)->first()->name);
            }
            $query->role = $roleName;
            return parent::sendResponse('data', $query, 'Profile Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function token(Request $request)
    {
        try {
            $data = (Auth::guard('api')->check()) ? Auth::guard('api')->user()->id : NULL;

            NotificationDevice::create([
                'device_id' => $request->expo_token,
                'user_id' =>  $data
            ]);

            return parent::sendResponse('status', "success", 'OK');
        } catch (\Exception $e) {
            return parent::sendError('Already Created', 215);
        }
    }

    public function modify_token_user(Request $request)
    {
        try {
            $data = ($request->type == 1) ? Auth::guard('api')->user()->id : NULL;

            NotificationDevice::where('device_id', $request->expo_token)->update([
                'user_id' =>  $data
            ]);
            return parent::sendResponse('status', "success", 'OK');
        } catch (\Exception $e) {
            return parent::sendError('Fail', 215);
        }
    }

    public function disable_notification(Request $request)
    {
        try {
            if (NotificationDevice::where('device_id', $request->expo_token)->delete()) {
                return parent::sendResponse('status', "success", 'OK');
            } else {
                return parent::sendResponse('status', $request->expo_token, 'OK');
            }
        } catch (\Exception $e) {
            return parent::sendError('Fail', 215);
        }
    }

    public function point_record()
    {
        try {
            $query = UserPointRecord::orderBy('created_at', 'desc')
                ->where('user_id', Auth::guard('api')->user()->id)
                ->get();
            return parent::sendResponse('data', $query, 'User Point Record');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }
}
