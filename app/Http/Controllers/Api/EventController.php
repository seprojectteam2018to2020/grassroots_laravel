<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Event\Event;
use App\Models\Event\EventUser;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class EventController extends ApiController
{

    public function index()
    {
        try {
            $query = Event::paginate(5);
            return parent::sendResponse('data', $query, 'Event Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function allevents()
    {
        try {
            $query = Event::all();
            return parent::sendResponse('data', $query, 'Event Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function show($id)
    {

        $user_id = Auth::guard('api')->user()->id;
        $query = Event::where('id', $id)->first();
        $limit_of_apply = $query->limit_of_apply;
        $number_of_apply = EventUser::where([
            ['status', '=', 1],
            ['event_id', '=', $id],
        ])->count();

        $event_status = "";

        if ($number_of_apply == $limit_of_apply) {
            $event_status = "full";
        } else {
            $event_status = "unfull";
        }


        $joincheck = EventUser::where([
            ['user_id', '=', $user_id],
            ['event_id', '=', $id],
        ])->count();




        if ($joincheck == 0) {
            if ($event_status == "full") {
                $query->button_status = "full";
            } else {
                $query->button_status = "canjoin";
            }
        } else {
            $status = EventUser::where([
                ['user_id', '=', $user_id],
                ['event_id', '=', $id],
            ])->first()->status;

            if ($status == 0) {
                $query->button_status = "waiting";
            } else if ($status == 1) {
                $query->button_status = "approved";
            } else if ($status == 2) {
                $query->button_status = "attend";
            } else {
                $query->button_status = "absent";
            }
        }

        return parent::sendResponse('data', $query, 'Event Data');
    }

    public function record()
    {
        try {
            $eventUserTB = config('variables.tables_name')['009'];
            $eventTB = config('variables.tables_name')['008'];

            $query = EventUser::where('user_id', Auth::guard('api')->user()->id)
                ->orderBy('created_at', 'desc')
                ->join($eventTB, "$eventUserTB.event_id", "=", "$eventTB.id")
                ->select("$eventTB.*")
                ->get();

            return parent::sendResponse('data', $query, 'Notification Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function join($id)
    {
        $user_id = Auth::guard('api')->user()->id;
        $event = Event::find($id);
        $limit_of_apply = $event->limit_of_apply;
        $number_of_apply = EventUser::where([
            ['status', '=', 1],
            ['event_id', '=', $id],
        ])->count();

        //check if the quota of event is full
        if ($number_of_apply < $limit_of_apply) {
            //new apply
            $new_event_record = EventUser::create([
                'event_id' => $id,
                'user_id' => $user_id,
                'status' => 0,

            ]);

            if (isset($new_event_record)) {
                return parent::sendResponse('status', "success", 'Joined');
            } else {
                return parent::sendError('Cannot join', 215);
            }
        } else {
            return parent::sendError('Full', 215);
        }
    }


    public function unjoin($id)
    {
        $user_id = Auth::guard('api')->user()->id;
        $event_id = $id;

        $unjoin = EventUser::where([
            ['user_id', '=', $user_id],
            ['event_id', '=', $event_id],
        ])->delete();

        if (isset($unjoin)) {
            return parent::sendResponse('status', 'success', 'unjoin');
        } else {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function participatelist($id)
    {
        try {
            $query = EventUser::where('event_id', $id)->get();
            return parent::sendResponse('data', $query, 'Participate Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function participate($id)
    {
        try {
            $user_id = Auth::guard('api')->user()->id;
            $user = User::find($user_id);
            $event = Event::find($id);

            $participate = EventUser::where("user_id", $user_id)->update([
                'status' => 1,
            ]);

            if (isset($participate)) {
                $reward_update = User::where('id', $user_id)->update([
                    'point' => $user->point + $event->reward
                ]);
            } else {
                parent::sendError('Participate Error', 215);
            }

            if (isset($reward_update)) {
                return parent::sendResponse('status', 'success', 'Participate Success');
            } else {
                parent::sendError('Update point Error', 215);
            }
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }
}
