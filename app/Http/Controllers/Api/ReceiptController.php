<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Receipt;
use Illuminate\Support\Facades\Auth;

class ReceiptController extends ApiController
{
    public function index()
    {

        try {
            $user_id = Auth::guard('api')->user()->id;

            $items = Receipt::select('id', 'title', 'transacted_at', 'amount')->where('user_id', $user_id)->orderBy('created_at', 'desc')->get();

            foreach ($items as $item) {
                $item->transacted_at = date('d F, Y', strtotime($item->transacted_at));
            }

            return parent::sendResponse('data', $items, 'Receipt Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function show($id)
    {
        $query = Receipt::where('id', $id)->first();
        $query->expense_string = config('variables.expense_type.' . $query->expense_type);
        $query->payment_string = config('variables.payment_type.' . $query->payment_type);
        $query->location_string = config('variables.location.' . $query->location);
        $query->currency_string = config('variables.currency.' . $query->currency_id);
        $query->date = $query->transacted_at;
        $query->transacted_at = date('d F, Y', strtotime($query->transacted_at));

        return parent::sendResponse('data', $query, 'Receipt Data');
    }

    public function create(Request $request)
    {

        if (!Auth::guard('api')->user()->hasRole('Member')) {
            return parent::sendError('Your account has not been approved, please waiting for authentication', 215);
        }

        $new_receipt = new Receipt;
        if (isset($request->receipt)) {
            $extension = $request->file('receipt')->getClientOriginalExtension();
            $fileNameToStore = str_random(16) . '_' . time() . '.' . $extension;
            $request->file('receipt')->storeAs(config('variables.folder.receipt'), $fileNameToStore);
            $new_receipt->image = $fileNameToStore;
        }

        $new_receipt->user_id =  Auth::guard('api')->user()->id;
        $new_receipt->transacted_at = $request->date;
        $new_receipt->payment_type = $request->payment_type;
        $new_receipt->expense_type = $request->expense_type;
        $new_receipt->currency_id = $request->currency_id;
        $new_receipt->amount = $request->amount;
        $new_receipt->remarks = $request->remarks;
        $new_receipt->title = $request->title;
        $new_receipt->location = $request->location;
        $new_receipt->save();

        if (isset($new_receipt)) {
            return parent::sendResponse('status', "success", 'Create successfully.');
        } else {
            return parent::sendError('Cannot create, please check your information again', 215);
        }
    }


    public function update(Request $request, $id)
    {
        $receipt = Receipt::find($id);

        if (isset($request->receipt)) {
            $extension = $request->file('receipt')->getClientOriginalExtension();
            $fileNameToStore = str_random(16) . '_' . time() . '.' . $extension;
            $request->file('receipt')->storeAs(config('variables.folder.receipt'), $fileNameToStore);
            $receipt->image = $fileNameToStore;
        }

        $receipt->transacted_at = $request->date;
        $receipt->payment_type = $request->payment_type;
        $receipt->expense_type = $request->expense_type;
        $receipt->currency_id = $request->currency_id;
        $receipt->amount = $request->amount;
        $receipt->remarks = $request->remarks;
        $receipt->title = $request->title;
        $receipt->location = $request->location;
        $receipt->save();

        if (isset($receipt)) {
            return parent::sendResponse('status', "success", 'Update successfully.');
        } else {
            return parent::sendError('Cannot Update, please check your information again', 215);
        }
    }

    public function destroy($id)
    {

        try {
            $receipt_id = Receipt::where("id", $id);

            if ($receipt_id->delete()) {
                return parent::sendResponse('status', "success", 'Delete successfully.');
            } else {
                return parent::sendError('Cannot Delete, please check your information again', 215);
            }
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function claim($id)
    {

        try {
            if (Receipt::findOrFail($id)->update([
                'status' => 0
            ])) {
                return parent::sendResponse('status', "success", 'Claim successfully.');
            } else {
                return parent::sendError('Cannot be claimed, please try again later', 215);
            }
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function list_claim()
    {

        try {
            $query = Receipt::whereNotNull('status')->where('user_id', Auth::guard('api')->user()->id)->get();
            return parent::sendResponse('data', $query, 'Claim Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }
}
