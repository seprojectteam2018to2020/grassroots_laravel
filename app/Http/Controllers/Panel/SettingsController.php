<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class SettingsController extends Controller
{
    public function index(){
        return view('admin.panel.settings.index');
    }

    public function git_pull()
    {
        $path_project = '/home/sm.anchorlab.it/public_html/social-membership';
        $path_script = '/resources/scripts/deploy.sh';

        if(!is_dir($path_project)) {
            session()->flash('fail', 'Error: Not found the directory location of the server.');
            return redirect()->back();
        }elseif (!file_exists($path_project.$path_script)){
            session()->flash('fail', 'Error: Not found the Shell file location of the server.');
            return redirect()->back();
        }

        $process = new Process("(cd $path_project; sh $path_project"."$path_script) ");
        $process->run();

        //executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $output = $process->getOutput();

        session()->flash('success', $output);
        return redirect()->back();
    }

}
