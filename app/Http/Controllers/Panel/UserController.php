<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\Settings\Currency;
use App\Models\UserPointRecord;
use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;
use App\Http\PushNotification;

class UserController extends Controller
{

    private $mode = [
        'isModeShow' => false,
        'isModeCreate' => false,
        'isModeDuplicate' => false,
        'isModeEdit' => false,
    ];

    function __construct()
    {
        $this->middleware('permission:user-list', ['only' => ['index']]);
        $this->middleware('permission:user-list|user-profile', ['only' => ['show']]);
        $this->middleware('permission:user-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user-edit|user-profile', ['only' => ['edit', 'update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.panel.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->mode['isModeCreate'] = true;
        $data = Role::all();
        foreach ($data as $item) {
            $roles[$item->id] = $item->name;
        }
        $currencies = Currency::all();
        return view('admin.panel.user.create', compact('roles', 'currencies'))->with('mode', $this->mode);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (User::where('email', $request->email)->get()) {
            return back()->withErrors('Email has been used');
        }

        $this->validate($request, User::rules());
        $user = new User;

        if (isset($request->avatar)) {
            $photoTypes = array('png', 'jpg', 'jpeg');
            $extension = $request->file('avatar')->getClientOriginalExtension();
            $isInFileType = in_array($extension, $photoTypes);

            if ($isInFileType) {
                $fileNameToStore = str_random(16) . '_' . time() . '.' . $extension;
                $request->file('avatar')->storeAs(config('variables.avatar.folder'), $fileNameToStore);
                $user->avatar = $fileNameToStore;
            } else {
                return back()->withErrors('Create Fail, Image type error, only png, jpg, jpeg');
            }
        }

        $user->name = serialize($request->name);
        $user->email = $request->email;
        $user->password = $request->password;
        $user->address = $request->address;
        $user->birth = $request->birth;
        $user->phone = $request->phone;
        $user->currency = $request->currency;
        $user->remarks = $request->remarks;
        $user->save();

        if (isset($request->role)) {
            $roles = Role::get();
            foreach ($roles as $role) {
                $user->removeRole($role->name);
                if (array_key_exists($role->id, $request->role)) {
                    $user->assignRole($role->name);
                }
            }
        }

        session()->flash('success', trans('app.success_store'));
        return redirect()->action('Panel\UserController@show', $user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->mode['isModeShow'] = true;
        $user = User::find($id);
        if (!$user) {
            abort(404);
        }
        if (!auth()->user()->hasPermissionTo('user-list')) {
            if ($id != auth()->user()->id) {
                abort(404);
            }
        }
        $roles = Role::get();
        $points = UserPointRecord::where('user_id', $id)->get();
        $currencies = Currency::all();
        return view('admin.panel.user.detail', compact('user', 'points', 'currencies', 'roles'))->with('mode', $this->mode);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->mode['isModeEdit'] = true;
        $user = User::find($id);
        if (!$user) {
            abort(404);
        }
        if (!auth()->user()->hasPermissionTo('user-list')) {
            if ($id != auth()->user()->id) {
                abort(404);
            }
        }
        $data = Role::all();
        $currencies = Currency::all();
        foreach ($data as $item) {
            $roles[$item->id] = $item->name;
        }
        return view('admin.panel.user.edit', compact('user', 'roles', 'currencies'))->with('mode', $this->mode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, User::rules(true, $id));
        $user = User::find($id);
        if (!$user) {
            abort(404);
        }
        if (!auth()->user()->hasPermissionTo('user-list')) {
            if ($id != auth()->user()->id) {
                abort(404);
            }
        }
        if (isset($request->avatar)) {
            $photoTypes = array('png', 'jpg', 'jpeg');
            $extension = $request->file('avatar')->getClientOriginalExtension();
            $isInFileType = in_array($extension, $photoTypes);

            if ($isInFileType) {
                $fileNameToStore = str_random(16) . '_' . time() . '.' . $extension;
                $request->file('avatar')->storeAs(config('variables.avatar.folder'), $fileNameToStore);
                $user->avatar = $fileNameToStore;
            } else {
                return back()->withErrors('Create Fail, Image type error, only png, jpg, jpeg');
            }
        }
        $user->name = serialize($request->name);
        $user->password = $request->password;
        $user->address = $request->address;
        $user->birth = $request->birth;
        $user->phone = $request->phone;
        $user->currency = $request->currency;
        $user->remarks = $request->remarks;
        $user->save();
        if (isset($request->role)) {
            $roles = Role::get();
            foreach ($roles as $role) {
                $user->removeRole($role->name);
                if (array_key_exists($role->id, $request->role)) {
                    $user->assignRole($role->name);
                }
            }
        }
        session()->flash('success', trans('app.success_update'));
        return redirect()->action('Panel\UserController@show', $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        session()->flash('success', trans('app.success_destroy'));
        return redirect()->action('Panel\UserController@index');
    }

    public function approve($id)
    {
        User::find($id)->update([
            'status' => 1,
            'added_at' => Carbon::now()
        ]);
        User::find($id)->assignRole('Member');

        PushNotification::send(
            User::find($id),
            'Your account has been approved',
            'Please check the detail in app',
            'Home',
            array()
        );

        UserPointRecord::create([
            'user_id' => $id,
            'content' => 'New User Rewards Package',
            'points' => 100,
            'type' => 1
        ]);

        User::find($id)->assignRole('Member');

        session()->flash('success', trans('Approve Succeed'));
        return redirect()->action('Panel\UserController@index');
    }

    public function ban($id)
    {
        User::find($id)->update([
            'status' => 2
        ]);

        PushNotification::send(
            User::find($id),
            'Your account has been banned',
            'Please check the detail in app',
            'Home',
            array()
        );

        session()->flash('success', trans('You banned the user'));
        return redirect()->action('Panel\UserController@index');
    }

    public function unban($id)
    {
        User::find($id)->update([
            'status' => 1
        ]);

        PushNotification::send(
            User::find($id),
            'Your account unlocked by adminstrator',
            'Please check the detail in app',
            'Home',
            array()
        );

        session()->flash('success', trans('You un-banned the user'));
        return redirect()->action('Panel\UserController@index');
    }
}
