<?php

namespace App\Http;

use App\Models\Notification\NotificationDevice;
use App\Models\Notification\NotificationUser;
use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Spatie\Permission\Models\Role;

class PushNotification
{

    public static function send($target, $title, $sub_title, $location, $params, $notificationID = NULL)
    {
        $data = [];
        $userTB = config('variables.tables_name')['003'];
        $deviceTB = config('variables.tables_name')['007'];
        $client = new Client();

        if (gettype($target) === 'string') {
            if ($target != "0") {
                $users = User::join($deviceTB, "$deviceTB.user_id", "=", "$userTB.id")
                    ->select("$userTB.id AS user_id", "$deviceTB.device_id")
                    ->role(Role::find($target)->name)
                    ->get();
            } else {
                $users = NotificationDevice::all();
            }

            foreach ($users as $user) {
                $data[] = array(
                    'to' => $user->device_id,
                    'title' => $title,
                    'body' => $sub_title,
                    'data' => array(
                        'location' => $location,
                        'params' => $params
                    ),
                    '_displayInForeground' => true,
                    'badge' => 1,
                    'priority' => 'high'
                );

                if ($user->user_id) {
                    NotificationUser::create([
                        'user_id' => $user->user_id,
                        'notification_id' => $notificationID,
                        'title' => $title,
                        'subtitle' => $sub_title,
                    ]);
                }
            }
        }

        if (gettype($target) === 'object' && $target->device != null) {
            $data[] = array(
                'to' => $target->device->device_id,
                'title' => $title,
                'body' => $sub_title,
                'data' => array(
                    'location' => $location,
                    'params' => $params
                ),
                '_displayInForeground' => true,
                'priority' => 'high'
            );

            NotificationUser::create([
                'user_id' => $target->id,
                'notification_id' => $notificationID,
                'title' => $title,
                'subtitle' => $sub_title,
            ]);
        }

        if (count($data) > 0) {
            $response = $client->post(
                'https://exp.host/--/api/v2/push/send',
                array(
                    'headers'  => ['content-type' => 'application/json'],
                    RequestOptions::JSON => $data,
                    'verify' => false,
                )
            );
        }
    }
}
