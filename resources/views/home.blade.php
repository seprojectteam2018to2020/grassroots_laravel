<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Styles -->
  <link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">
</head>

<body class="app">

  @yield('content')


  <!-- ### $App Screen Footer ### -->
  <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600 fixed-bottom">
    <span>Copyright © 2019 Designed by <a href="https://colorlib.com" target='_blank' title="Colorlib">Colorlib</a>. All
      rights reserved.</span>
  </footer>

</body>

</html>