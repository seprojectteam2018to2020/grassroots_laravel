@extends('admin.default')

@section('page-header')
    Feedback <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')

    @if(auth()->user()->hasRole('Admin') || $mode['isUserList'])
        <div class="row">
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Feedback ID</th>
                                @if(!$mode['isUserList'])
                                    <th>User</th>
                                @endif
                                <th>Priority</th>
                                <th>Type</th>
                                <th>Comment</th>
                                <th>Status</th>
                                <th>Created Date</th>
                                <th class="align-middle text-center">Updated At</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th>Feedback ID</th>
                                @if(!$mode['isUserList'])
                                    <th>User</th>
                                @endif
                                <th>Priority</th>
                                <th>Type</th>
                                <th>Comment</th>
                                <th>Status</th>
                                <th>Created Date</th>
                                <th class="align-middle text-center">Updated At</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>

                            <tbody>
                            @foreach ($feedbacks as $feedback)
                                <tr>
                                    <td class="align-middle text-center">{{$feedback->id}}</td>
                                    @if(!$mode['isUserList'])
                                        <td class="align-middle text-center">{{$feedback->user->email}}</td>
                                    @endif
                                    <td class="align-middle text-center">
                                        @switch($feedback->priority)
                                            @case(config("constants.FEEDBACK_PRIORITY.Pending"))
                                            <span class='badge badge-primary'>Pending</span>
                                            @break
                                            @case(config("constants.FEEDBACK_PRIORITY.LOW"))
                                            <span class='badge badge-success'>Low</span>
                                            @break
                                            @case(config("constants.FEEDBACK_PRIORITY.MEDIUM"))
                                            <span class='badge badge-warning'>Medium</span>
                                            @break
                                            @case(config("constants.FEEDBACK_PRIORITY.HIGH"))
                                            <span class='badge badge-danger'>High</span>
                                            @break
                                            @default
                                            <span class='badge badge-dark text-light'>Unknown</span>
                                        @endswitch
                                    </td>
                                    <td class="align-middle text-center">{{$feedback->feedbackType->name}}</td>
                                    <td class="align-middle text-center">{{count($feedback->chat)}}</td>
                                    <td class="align-middle text-center">
                                        @switch($feedback->status)
                                            @case(config("constants.FEEDBACK_STATUS.OPEN"))
                                            <span class='badge badge-success'>Open</span>
                                            @break
                                            @case(config("constants.FEEDBACK_STATUS.COMPLETED"))
                                            <span class='badge badge-warning'>Completed</span>
                                            @break
                                            @case(config("constants.FEEDBACK_STATUS.CLOSED"))
                                            <span class='badge badge-secondary'>Closed</span>
                                            @break
                                            @case(config("constants.FEEDBACK_STATUS.CANCELLED"))
                                            <span class='badge badge-danger'>Cancelled</span>
                                            @break
                                            @default
                                            <span class='badge badge-dark text-light'>Unknown</span>
                                        @endswitch
                                    </td>
                                    <td class="align-middle text-center">{{$feedback->created_at}}</td>
                                    <td class="align-middle text-center">{{$feedback->updated_at->diffForHumans()}}</td>
                                    <td class="align-middle text-center">
                                        <a href="{{ URL::action('Panel\FeedbackController@show', $feedback->id) }}" class="btn btn-primary"><i class="ti-eye"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="" style="min-height: 100%;min-height: 60vh;display: flex;align-items: center;">
            <div class="container">
                <div class="row">
                    <div class="card text-center m-auto" style="width: 30rem;">
                        <div class="card-body">
                            <h5 class="card-title h1">Create Feedback</h5>
                            <p class="card-text display-3"><i class="ti-headphone-alt"></i></p>
                            <a href="{{ URL::action('Panel\FeedbackController@create')  }}" class="btn btn-primary">Create Now</a>
                        </div>
                    </div>
                    <div class="card text-center m-auto" style="width: 30rem;">
                        <div class="card-body">
                            <h5 class="card-title h1">My Feedback</h5>
                            <p class="card-text display-3"><i class="ti-comment-alt"></i></p>
                            <a href="{{ URL::action('Panel\FeedbackController@user_list',auth()->user()->id)  }}" class="btn btn-primary">List All</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection

