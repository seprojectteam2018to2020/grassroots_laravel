@extends('admin.default')

@section('page-header')
User Reward <small>{{ $user->name['first'].' '.$user->name['last'].' - '.trans('app.add_new_item') }}</small>
@stop

@section('content')
{!! Form::model($point,[
  'action' => ['Panel\RewardController@update',$user->id,$point->id],
  'method' => 'put',
  'files' => true ])
!!}

@include('admin.panel.reward.partial.form')

<div class="text-center">
  <button type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
  <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
</div>

{!! Form::close() !!}


@stop