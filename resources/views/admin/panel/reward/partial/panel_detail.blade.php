{{-- Panel --}}
@if($mode['isModeShow'])
<div class="row mB-10">
  <div class="col-sm-10 m-auto">
    <div class="bgc-white p-20 bd">

      @can('reward-create')
      <a href="{{ URL::action('Panel\RewardController@add', $user->id) }}" class="btn cur-p btn-success">Add</a>
      @endcan

      <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
    </div>
  </div>
</div>
@endif