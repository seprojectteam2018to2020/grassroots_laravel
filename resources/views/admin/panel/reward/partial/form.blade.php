<div class="row mB-40">
  <div class="col-sm-10 m-auto">
    <div class="bgc-white p-20 bd">
      {!! Form::myInput('email', 'email', 'Email', ['class'=>'form-control form-control-plaintext text-success
      p-0','disabled','required']) !!}
      <input type="hidden" name="id" value="{{$user->id}}">
      <div class="form-row">
        <div class="col-2">
          <div class="form-group">
            {!! Form::label('type', 'Type') !!}
            <select name="type" class="form-control" @if($mode['isModeShow'])disabled="disabled" @endif>
              <option value="1" @if($mode['isModeShow'] || $mode['isModeEdit'])
                @if($point['type']==1)selected="selected" @endif @endif>Add</option>
              <option value="0" @if($mode['isModeShow'] || $mode['isModeEdit'])
                @if($point['type']==0)selected="selected" @endif @endif>Minus</option>
            </select>
          </div>
        </div>
        <div class="col">
          <div class="form-group">
            {!! Form::label('point', 'Point') !!}
            <input type="number" name="point" class="form-control"
              value="{{(($mode['isModeEdit']) || ($mode['isModeShow']))?$point->points:0}}" @if(($mode['isModeShow']))
              disabled @endif required min="0">
          </div>
        </div>
      </div>

      {!! Form::myTextArea('content', 'Content', ($mode['isModeShow'])?['disabled','required']:['required']) !!}
    </div>
  </div>
</div>