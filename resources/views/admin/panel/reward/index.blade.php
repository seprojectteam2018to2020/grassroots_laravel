@extends('admin.default')

@section('page-header')
Users Reward Record <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="bgc-white bd bdrs-3 p-20 mB-20">
      <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">

        <thead>
          <tr>
            <th>User ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Point</th>
            <th>Update At</th>
          </tr>
        </thead>

        <tfoot>
          <tr>
            <th>User ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Point</th>
            <th>Update At</th>
          </tr>
        </tfoot>

        <tbody>
          @foreach ($users as $user)
          <tr>
            <td>{{ $user->id }}</td>
            <td>
              <a href="{{ URL::action('Panel\RewardController@show', $user['id']) }}">{{ $user->name['first'].' '.$user->name['last'] }}</a>
            </td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->point }}</td>
            <td>
              @foreach($points as $point)
                @if($point->user_id==$user->id)
                  {{$point->updated_at}}
                  @break
                @endif
              @endforeach
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

