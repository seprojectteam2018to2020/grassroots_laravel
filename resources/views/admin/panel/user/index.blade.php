@extends('admin.default')

@section('page-header')
Users <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')

<div class="mB-20">
  <a href="{{ URL::action('Panel\UserController@create') }}" class="btn btn-primary">
    {{ trans('app.add_button') }}
  </a>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="bgc-white bd bdrs-3 p-20 mB-20">
      <table id="dataTable" class="table table-bordered" cellspacing="0" width="100%">

        <thead>
          <tr>
            <th>Status</th>
            <th>Name</th>
            <th>Email</th>
            <th>Actions</th>
          </tr>
        </thead>

        <tbody>
          @foreach ($users as $user)
          <tr>
            <td>
              @if($user->status == 0)
              <span class="badge badge-warning">
                @elseif($user->status == 1)
                <span class="badge badge-info">
                  @elseif($user->status == 2)
                  <span class="badge badge-danger">
                    @endif
                    {{config('variables.user_status')[$user->status]}}</span>
            </td>
            <td>
              <a href=" {{ URL::action('Panel\UserController@show', $user['id']) }}">
                {{ $user->name['first'].' '.$user->name['last'] }}</a>
            </td>
            <td>{{ $user->email }}</td>
            <td>
              <ul class="list-inline">
                <li class="list-inline-item">
                  <a href="{{ URL::action('Panel\UserController@show', $user['id']) }}" title="Show User"
                    class="btn btn-secondary btn-sm">
                    <span class="ti-align-justify"></span>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a href="{{ URL::action('Panel\UserController@edit', $user['id']) }}"
                    title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm">
                    <span class="ti-pencil"></span>
                  </a>
                </li>
                @if($user['id']!=auth()->user()->id)
                <li class="list-inline-item">
                  {!! Form::open([
                  'class'=>'delete',
                  'url' => URL::action('Panel\UserController@destroy', $user['id']),
                  'method' => 'DELETE',
                  ])
                  !!}

                  <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}"><i
                      class="ti-trash"></i></button>

                  {!! Form::close() !!}
                </li>
                @endif
              </ul>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection