@extends('admin.default')

@section('page-header')
User <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
{!!
Form::model($user, [
'action' => ['Panel\UserController@update', $user->id],
'method' => 'put',
'files' => true
])
!!}

@include('admin.panel.user.partial.form')

<div class="text-center">
	<button type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
	<a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
</div>

{!! Form::close() !!}

@stop