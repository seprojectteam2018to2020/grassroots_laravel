@extends('admin.default')

@section('page-header')
Receipt <small>{{ trans('app.detail_item') }}</small>
@stop


@section('content')

<div class="row mB-10">
    <div class="col-sm-10 m-auto">
        <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
    </div>
</div>


<div class="row mB-40">
    <div class="col-sm-10 m-auto">
        {!! Form::model($receipts) !!}
        <div class="bgc-white p-20 bd mb-5">
            <div>
                {!! Form::myInput('text', 'title', 'Title', ['disabled','required']) !!}
            </div>

            <div>
                {!! Form::myInput('text', 'transacted_at', 'Transacted at',
                ['disabled','required']) !!}
            </div>

            <div>
                {!! Form::myInput('text', 'currency_id', 'Currency',
                ['disabled','required']) !!}
            </div>

            <div>
                {!! Form::myInput('text', 'location', 'Location',
                ['disabled','required']) !!}
            </div>

            <div>
                {!! Form::myInput('text', 'expense_type', 'Expense type',
                ['disabled','required']) !!}
            </div>

            <div>
                {!! Form::myInput('text', 'payment_type', 'Payment type',
                ['disabled','required']) !!}
            </div>

            <div>
                {!! Form::myInput('text', 'amount', 'Amount',
                ['disabled','required']) !!}
            </div>

            <div>
                {!! Form::myInput('text', 'remarks', 'Remark',
                ['disabled','required']) !!}
            </div>

        </div>
        {!! Form::close() !!}

        <div class="card">
            <div class="card-header">Approvement</div>
            <div class="card-body">
                @if($receipts->status == 0)
                {!! Form::model($receipts, [
                'action' => ['Panel\ReceiptController@approve', $receipts->id],
                'method' => 'POST'])
                !!}
                <div class="row">
                    <div class="col">
                        {!! Form::mySelect('status', 'Action'.' <span style="color:red">*</span>',
                        array(
                        '3' => 'Approve',
                        '2' => 'Disapproved') , null
                        ,array('required' => 'required'))!!}
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        {!! Form::myTextArea('comment', 'Comment'.' <span style="color:red">*</span>') !!}
                    </div>
                </div>
                @else
                {!! Form::model($receipts,[
                'action' => ['Panel\ReceiptController@update',$receipts->id],
                'files' => true,
                'method' => 'PUT', ])
                !!}
                <div class="row">
                    <div class="col">
                        {!! Form::myTextArea('comment', 'Comment'.' <span style="color:red">*</span>') !!}
                    </div>

                </div>
                @endif
                <button type="submit" class="btn btn-primary">Send</button>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
</div>



@stop