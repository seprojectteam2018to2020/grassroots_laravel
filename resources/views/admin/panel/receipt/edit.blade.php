@extends('admin.default')

@section('page-header')
Receipt <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
{!! 
	Form::model($receipts, [
		'action' => ['Panel\ReceiptController@update', $receipts->id],
		'method' => 'put',
		'files' => true
	])
!!}

@include('admin.panel.receipt.partial.form')

	<div class="text-center">
		<button type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
		<a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
	</div>

{!! Form::close() !!}

@stop