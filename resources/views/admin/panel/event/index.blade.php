@extends('admin.default')

@section('page-header')
Event&nbsp;<small>{{ trans('app.manage') }}</small>
@endsection

@section('content')
<div class="mB-20">
  <a href="{{ URL::action('Panel\EventController@create') }}" class="btn btn-info">
    {{ trans('app.add_button') }}
  </a>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="bgc-white bd bdrs-3 p-20 mB-20">
      <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">

        <thead>
          <tr>
            <th style="width:140px">Type</th>
            <th style="width:auto">Title</th>
            <th style="width:auto">Content</th>
            <th style="width:130px">Action</th>
          </tr>
        </thead>

        <tbody>
          @foreach ($events as $event)
          <tr>
            <td>
              {{config('variables.event_type')[$event->type]}}
            </td>
            <td>
              <a href="{{ URL::action('Panel\EventController@show', $event['id']) }}">
                {{ $event['title'] }}
              </a>
            </td>
            <td>{{ $event['content'] }}</td>

            <td>
              <ul class="list-inline">
                <li class="list-inline-item">
                  <a href="{{ URL::action('Panel\EventController@edit', $event['id']) }}"
                    title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm">
                    <span class="ti-pencil"></span>
                  </a>
                </li>

                <li class="list-inline-item">
                  {!! Form::open([
                  'class'=>'delete',
                  'url' => URL::action('Panel\EventController@destroy', $event['id']),
                  'method' => 'DELETE', ])
                  !!}
                  <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}">
                    <i class="ti-trash"></i>
                  </button>
                  {!! Form::close() !!}
                </li>


              </ul>
            </td>
          </tr>

          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection