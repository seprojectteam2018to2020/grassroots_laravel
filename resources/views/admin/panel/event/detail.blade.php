@extends('admin.default')

@section('page-header')
Event - <small>{{ $events->title.' - '.trans('app.detail_item') }}</small>
@stop

@section('content')

@include('admin.panel.event.partial.panel_detail')

{!! Form::model($events) !!}
@include('admin.panel.event.partial.form')
{!! Form::close() !!}

@stop