@extends('admin.default')

@section('page-header')
Event&nbsp;<small>{{ trans('app.add_new_item') }}</small>
@stop

@section('content')
{!! Form::open([
'action' => ['Panel\EventController@store'],
'files' => true,
])
!!}

@include('admin.panel.event.partial.form')

<div class="text-center">
  <button type="submit" class="btn btn-primary">{{ trans('app.add_button') }}</button>
  <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
</div>

{!! Form::close() !!}

@stop