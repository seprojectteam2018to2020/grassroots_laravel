{{-- Panel --}}
@if($mode['isModeShow'])
<div class="row mB-10">
  <div class="col-sm-10 m-auto">
    <div class="bgc-white p-20 bd">

      @can('event-create')
      <a href="{{ URL::action('Panel\EventController@create') }}" class="btn cur-p btn-success">New</a>
      @endcan

      @can('event-edit')
      <a href="{{ URL::action('Panel\EventController@edit',$events->id) }}" class="btn cur-p btn-warning">Edit</a>
      @endcan

      @can('event-delete')
      {!! Form::open([
				'class'=>'delete d-inline',
				'url' => URL::action('Panel\EventController@destroy', $events['id']),
				'method' => 'DELETE', ])
      !!}
      <button class="btn btn-danger" title="{{ trans('app.delete_title') }}">Delete</button>
      {!! Form::close() !!}
      @endcan
      
      <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
    </div>
  </div>
</div>
@endif