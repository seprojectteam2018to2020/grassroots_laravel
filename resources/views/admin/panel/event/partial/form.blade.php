<div class="row mB-40">
  <div class="col-sm-10 m-auto">
    <div class="bgc-white p-20 bd">

      <div>
        {!! Form::myInput('text', 'title', 'Title',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myTextArea('content', 'Content',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div class="form-group">
        {!! Form::label('type', 'Type') !!}
        <select name="type" class="form-control" @if($mode['isModeShow']) disabled="disabled" @endif required>


          @if ($mode['isModeCreate'])
          <option value="" disabled selected>Select your option</option>
          @foreach($eventTypes as $eventType)
          <option value="{{$eventType->id}}">{{$eventType->type}}</option>
          @endforeach


          @elseif($mode['isModeShow'])
          <option value="{{$events->type}}">{{ config('variables.event_type')[$events->type]}}</option>


          @elseif($mode['isModeEdit'])
          @foreach($eventTypes as $eventType)
          <option value="{{$eventType->id}}" @if($eventType->id == $events->type) selected="selected" @endif
            >{{$eventType->type}}</option>
          @endforeach
          @endif
        </select>
      </div>

      <div>
        {!! Form::myInput('number', 'reward', 'reward',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myInput('text', 'venue', 'Venue',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myInput('date', 'valid_from', 'Valid From',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myInput('date', 'valid_to', 'Valid To',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      @if($mode['isModeCreate'] || $mode['isModeEdit'])
      <div>
        {!! Form::myInput('date', 'date', 'Start On',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myInput('time', 'time_from', 'From',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myInput('time', 'time_to', 'To',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>
      @else
      <div>
        {!! Form::myInput('text', 'time', 'Time', ['disabled','required']) !!}
      </div>
      @endif

      <div>
        {!! Form::myInput('number', 'limit_of_apply', 'Limit of apply',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div class="text-center">
        @if($mode['isModeShow'])
        <img id="image" width="100%" src="{{$events->image}}">
        @elseif($mode['isModeEdit'])
        <img id="image" width="100%" src="{{$events->image}}">
        {!! Form::myFile('image', 'Event Image' ,['accept'=>'image/*']) !!}
        @else
        {!! Form::myFile('image', 'Event Image' ,['accept'=>'image/*']) !!}
        @endif
      </div>

    </div>
  </div>
</div>