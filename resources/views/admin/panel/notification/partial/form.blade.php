<div class="row mB-40">
  <div class="col-sm-10 m-auto">
    <div class="bgc-white p-20 bd">

      <div>
        {!! Form::myInput('text', 'title', 'Title',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myTextArea('content', 'Content',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myInput('date', 'date', 'Date',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myInput('time', 'time', 'Time',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::mySelect('type', 'Type',
        config('variables.notification_type'), null
        ,($mode['isModeShow'])?['disabled',' required']:['required'])!!}
      </div>

      <div class="text-center">
        @if($mode['isModeShow'] && !preg_match('/.*\/$/',$notifications->image))
        <img id="image" width="100%" src="{{$notifications->image}}">
        @elseif($mode['isModeEdit'])
        <img id="image" width="100%" src="{{$notifications->image}}">
        {!! Form::myFile('image', 'Notification Image' ,['accept'=>'image/*']) !!}
        @else
        {!! Form::myFile('image', 'Notification Image' ,['accept'=>'image/*']) !!}
        @endif
      </div>

    </div>
  </div>
</div>