{{-- Panel --}}
@if($mode['isModeShow'])
<div class="row mB-10">
  <div class="col-sm-10 m-auto">
    <div class="bgc-white p-20 bd">

      @can('notification-create')
      <a href="{{ URL::action('Panel\NotificationController@create') }}" class="btn cur-p btn-success">New</a>
      @endcan

      @can('notification-edit')
      <a href="{{ URL::action('Panel\NotificationController@edit',$notifications->id) }}"
        class="btn cur-p btn-warning">Edit</a>
      @endcan

      @can('notification-delete')
      {!! Form::open([
      'class'=>'delete d-inline',
      'url' => URL::action('Panel\NotificationController@destroy', $notifications['id']),
      'method' => 'DELETE', ])
      !!}
      <button class="btn btn-danger" title="{{ trans('app.delete_title') }}">Delete</button>
      {!! Form::close() !!}
      @endcan

      <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>

    </div>
  </div>
</div>
@endif