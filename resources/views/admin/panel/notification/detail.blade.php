@extends('admin.default')

@section('page-header')
Notification <small>{{ trans('app.detail_item') }}</small>
@stop


@section('content')

@include('admin.panel.notification.partial.panel_detail')

{!! Form::model($notifications) !!}
@include('admin.panel.notification.partial.form')
{!! Form::close() !!}

@stop

