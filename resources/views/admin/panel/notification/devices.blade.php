@extends('admin.default')

@section('page-header')
Notification Devices <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')

<div class="row mB-10">
    <div class="col-sm-10">
        <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
            <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">

                <thead>
                    <tr>
                        <th>Devices ID</th>
                        <th>Belongs To</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($devices as $device)
                    <tr>
                        <td>{{ $device->device_id }}</td>
                        <td>
                            @if(is_null($device->user_id))
                            <span>-</span>
                            @else
                            {{$device->user->email}}
                            @endif
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection