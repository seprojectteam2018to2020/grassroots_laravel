@php
$r = \Route::current()->getAction();
$route = (isset($r['as'])) ? $r['as'] : '';
@endphp

{{-- <li class="nav-item mT-30">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.dash') ? 'active' : '' }}"
href="{{ URL::action('Panel\DashboardController@index') }}">
<span class="icon-holder">
    <i class="c-blue-500 ti-home"></i>
</span>
<span class="title">Dashboard</span>
</a>
</li> --}}
<li class="nav-item mT-30">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.user') ? 'active' : '' }}"
        href="@can('user-list'){{ URL::action('Panel\UserController@index') }}@else {{ URL::action('Panel\UserController@show',auth()->user()->id) }} @endcan">
        <span class="icon-holder">
            <i class="c-brown-500 ti-user"></i>
        </span>
        <span class="title">User</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.event') ? 'active' : '' }}"
        href="@can('event-list'){{ URL::action('Panel\EventController@index') }}@else {{ URL::action('Panel\EventController@show',auth()->user()->id) }} @endcan">
        <span class="icon-holder">
            <i class="c-brown-500 ti-notepad"></i>
        </span>
        <span class="title">Event</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.reward') ? 'active' : '' }}"
        href="@can('reward-list'){{ URL::action('Panel\RewardController@index') }}@else {{ URL::action('Panel\RewardController@show',auth()->user()->id) }} @endcan">
        <span class="icon-holder">
            <i class="c-brown-500 ti-cup"></i>
        </span>
        <span class="title">Reward</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.shop') ? 'active' : '' }}"
        href="{{ URL::action('Panel\ShopController@index') }}">
        <span class="icon-holder">
            <i class="c-brown-500 ti-archive"></i>
        </span>
        <span class="title">Shop</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.feedback') ? 'active' : '' }}"
        href="@can('feedback-list'){{ URL::action('Panel\FeedbackController@index') }} @endcan">
        <span class="icon-holder">
            <i class="c-brown-500 ti-comment-alt"></i>
        </span>
        <span class="title">Feedback</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.notification') ? 'active' : '' }}"
        href="@can('notification-list'){{ URL::action('Panel\NotificationController@index') }}@else {{ URL::action('Panel\NotificationController@index') }} @endcan">
        <span class="icon-holder">
            <i class="c-brown-500 ti-announcement"></i>
        </span>
        <span class="title">Notification</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.receipt') ? 'active' : '' }}"
        href="@can('receipt-list'){{ URL::action('Panel\ReceiptController@index') }}@else {{ URL::action('Panel\ReceiptController@show',auth()->user()->id) }} @endcan">
        <span class="icon-holder">
            <i class="c-brown-500 ti-agenda"></i>
        </span>
        <span class="title">Receipt</span>
    </a>
</li>
@if(auth()->user()->hasRole('Admin'))
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.settings') ? 'active' : '' }}"
        href="{{ URL::action('Panel\SettingsController@index') }}">
        <span class="icon-holder">
            <i class="c-brown-500 ti-settings"></i>
        </span>
        <span class="title">Settings</span>
    </a>
</li>
@endif