<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('v1')->group(function () {
    Route::group(['middleware' => ['auth:api']], function () {

        // url link: http://domain/api/v1/test
        Route::get('test', 'Api\TestController@index');
        Route::post('modify-token-user', 'Api\AuthController@modify_token_user');
        Route::post('disable-notification', 'Api\AuthController@disable_notification');

        //notification
        Route::get('notification', 'Api\NotificationController@index');
        Route::get('notification/{id}', 'Api\NotificationController@show');
        Route::get('notification-personal', 'Api\NotificationController@personal');
        Route::delete('notification-personal/{id}', 'Api\NotificationController@personal_delete');
        Route::post('notification/read/{id}', 'Api\NotificationController@read');


        //receipt
        Route::get('receipt', 'Api\ReceiptController@index');
        Route::get('receipt/{id}', 'Api\ReceiptController@show');
        Route::get('receipt/edit/{id}', 'Api\ReceiptController@edit');
        Route::post('receipt', 'Api\ReceiptController@create');
        Route::post('receipt/{id}', 'Api\ReceiptController@update');
        Route::delete('receipt/{id}', 'Api\ReceiptController@destroy');
        Route::post('receipt-claim/{id}', 'Api\ReceiptController@claim');
        Route::get('list-claim', 'Api\ReceiptController@list_claim');

        //event
        Route::get('event', 'Api\EventController@index');
        Route::get('event-record', 'Api\EventController@record');
        Route::get('event/{id}', 'Api\EventController@show');
        Route::post('event/{id}', 'Api\EventController@join');
        Route::delete('event/{id}', 'Api\EventController@unjoin');
        Route::get('all-events', 'Api\EventController@allevents');

        //event (admin)
        Route::get('event/participate/{id}', 'Api\EventController@participatelist');
        Route::post('event/participate/{id}', 'Api\EventController@participate');

        //feedback
        Route::post('feedback', 'Api\FeedbackController@create');
        Route::get('feedback-type-list', 'Api\FeedbackController@feedback_type_list');
        //feedback (admin)
        Route::get('feedback', 'Api\FeedbackController@index');

        //profile
        Route::get('profile', 'Api\AuthController@profile');
        Route::post('profile', 'Api\AuthController@edit_profile');
        Route::get('point-record', 'Api\AuthController@point_record');

        //shop
        Route::get('shop', 'Api\ShopController@index');
        Route::post('shop/{id}', 'Api\ShopController@exchange');

        //admin
        Route::group(['prefix' => 'admin', 'middleware' => ['role:Admin']], function () {
            Route::get('user/list', 'Api\AdminController@user_list');
            Route::get('user/show/{id}', 'Api\AdminController@user_show');
            Route::post('user/approve/{id}', 'Api\AdminController@user_approve');
            Route::post('user/ban/{id}', 'Api\AdminController@user_ban');
            Route::post('user/unban/{id}', 'Api\AdminController@user_unban');

            Route::get('account/list', 'Api\AdminController@receipt_list');
            Route::get('account/show/{id}', 'Api\AdminController@receipt_show');
            Route::post('account/comment/{id}', 'Api\AdminController@receipt_comment');
        });
    });

    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@register');
    Route::post('token', 'Api\AuthController@token');

    //event
    Route::get('/unlogin/event', 'Api\EventController@index');

    //notice
    Route::get('/unlogin/notification', 'Api\NotificationController@index');
});


Route::prefix('v2')->group(function () {
    Route::group(['middleware' => ['auth:api']], function () {
        //
    });
});
