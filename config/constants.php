<?php
/**
 * Created by PhpStorm.
 * User: petercyyau
 * Date: 17/9/2018
 * Time: 15:14
 */

return [


    'FEEDBACK_STATUS' => [
        'OPEN' => 10,
        'COMPLETED' => 20,
        'CLOSED' => 30,
        'CANCELLED' => 40,
    ],

    'FEEDBACK_PRIORITY' => [
        'UNKNOWN' => 0,
        'LOW' => 10,
        'MEDIUM' => 20,
        'HIGH' => 30,
    ],
];